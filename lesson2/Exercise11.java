import java.util.Scanner;

public class Exercise11 {
    public static void main(String[] args) {
        //задача:"Выведите на экран все положительные делители натурального числа, введённого пользователем с клавиатуры."

        int n = -1;
        Scanner sc = new Scanner(System.in);
        while (n <= 0) {
            System.out.println("Введите натуральное число:");
            while (!sc.hasNextInt()) {
                System.out.println("Введите натуральное число:");
                sc.next();
            }
            n = sc.nextInt();
        }

        //алгоритм перебора делителей
        System.out.println("Положительные делители числа " + n + ":");

        int j = 1;
        for (int i = 0; i <= n/2; i++) {
            while (n % j == 0) {
                System.out.println(j);
                j++;
            }
            j++;
        }
        System.out.println(n);


    }
}
