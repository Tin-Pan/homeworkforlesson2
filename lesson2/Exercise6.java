public class Exercise6 {
    public static void main(String[] args) {
        //задание:"Создайте программу, выводящую на экран все четырёхзначные числа последовательности 1000 1003 1006 1009 1012 1015"

        int a = 1000;
        while (a < 10000) {
            System.out.print(a + " ");
            a += 3;
        }
    }
}
