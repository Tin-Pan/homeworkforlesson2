import java.util.Scanner;

public class Exercise10 {
    public static void main(String[] args) {
        //задание:"Создайте программу, вычисляющую факториал натурального числа n, которое пользователь введёт с клавиатуры."

        //кроме проверки на int сделаем проверку что число положительное
        int n = -1;
        Scanner sc = new Scanner(System.in);
        while (n < 0) {
            System.out.println("Введите натуральное число:");
            while (!sc.hasNextInt()) {
                System.out.println("Введите натуральное число:");
                sc.next();
            }
            n = sc.nextInt();
        }
        //System.out.println(n);

        //вычисляем факториал
        int x = n;
        long result = 1L;
        while (x > 0) {
            result *= x;
            x -= 1;
        }

        System.out.println("Факториал числа " + n + " равен " + result);

    }
}
