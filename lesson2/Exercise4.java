import java.util.Random;

public class Exercise4 {
    public static void main(String[] args) {
        //задание:"Создать программу, выводящую на экран случайно сгенерированное трёхзначное натуральное число и его наибольшую цифру."

        Random rnd = new Random();
        int a = rnd.nextInt(899) + 100;

        //Вычисляем цифры в сгенерированном числе
        int a1 = a % 10;
        int a2 = (a / 10) % 10;
        int a3 = (a / 100) % 10;

        //Ищем максимальное
        int amax;
        if (a1 >= a2 && a1 >= a3) {
            amax = a1;
        } else if (a2 >= a3) {
            amax = a2;
        } else {
            amax = a3;
        }

        System.out.println("В числе " + a + " наибольшая цифра " + amax + ".");
    }
}
