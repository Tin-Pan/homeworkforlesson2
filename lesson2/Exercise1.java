
import java.util.Scanner;

public class Exercise1 {

    public static void main(String[] args) {

        //задание:"Создать программу, проверяющую и сообщающую на экран, является ли целое число записанное в переменную n, чётным либо нечётным."

        //программа считывает с консоли введенное число, проверяет что оно int, затем выводит результат
        //чётность проверяется с помощью деления на 2 и получения от этого деления остатка (оператор %)
        //если остаток равен 0 - чётное, иначе - нечётное

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите целое число:");
        while (!sc.hasNextInt()) {
            System.out.println("Введите целое число:");
            sc.next();
        }
        int n = sc.nextInt();

        if (n % 2 == 0) {
            System.out.println("Число четное.");
        } else {
            System.out.println("Число нечетное.");
        }


    }
}
