import java.util.Scanner;

public class Exercise12 {
    public static void main(String[] args) {
        //задание:"Проверьте, является ли введённое пользователем с клавиатуры натуральное число — простым.
        //Постарайтесь не выполнять лишних действий (например, после того, как вы нашли хотя бы один нетривиальный
        //делитель уже ясно, что число составное и проверку продолжать не нужно). Также учтите, что наименьший делитель
        //натурального числа n, если он вообще имеется, обязательно располагается в отрезке [2; √n]."

        int n = -1;
        Scanner sc = new Scanner(System.in);
        while (n <= 0) {
            System.out.println("Введите натуральное число:");
            while (!sc.hasNextInt()) {
                System.out.println("Введите натуральное число:");
                sc.next();
            }
            n = sc.nextInt();
        }

        long q = Math.round(Math.sqrt(n));
        int i = 2;
        int j = 0;
        while (i * i <= q && j != 1) {
            while (n % i == 0) {
                System.out.println("Введено составное число");
                j = 1;
            }
            i += 1;
        }
        if (j == 0) {
            System.out.println("Введено простое число");
        }


    }
}
