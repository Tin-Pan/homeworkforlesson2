import java.util.Random;

public class Exercise3 {
    public static void main(String[] args) {

        //задание:"Создать программу, которая будет проверять попало ли случайно выбранное из отрезка [5;155] целое число в интервал (25;100) и сообщать результат на экран.

        Random rnd = new Random();
        int a = rnd.nextInt(150) + 5;

        if (a >25 && a <100){
            System.out.println("Число " + a + " содержится в интервале (25,100).");
        } else {
            System.out.println("Число " + a + " не содержится в интервале (25,100).");
        }

    }
}
