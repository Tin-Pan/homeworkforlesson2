import java.util.Scanner;

import static java.lang.Math.abs;

public class Exercise2 {
    public static void main(String[] args) {

        //задание:"Создать программу, выводящую на экран ближайшее к 10 из двух чисел, записанных в переменные m и n."

        //программа считывает с консоли введенные числа, проверяет что они float, затем выводит результат
        //ближайшее число к 10ти проверяем так: abs(10-m) == abs(10-n) - числа одинаково близки от 10,
        //abs(10-m) > abs(10-n) - ближе число n, иначе ближе m

        Scanner sc1 = new Scanner(System.in);
        System.out.println("Введите число m:");
        while (!sc1.hasNextFloat()) {
            System.out.println("Введите число:");
            sc1.next();
        }
        float m = sc1.nextFloat();

        //Scanner sc2 = new Scanner(System.in);
        System.out.println("Введите число n:");
        while (!sc1.hasNextFloat()) {
            System.out.println("Введите число:");
            sc1.next();
        }
        float n = sc1.nextFloat();

        if (abs(10 - m) == abs(10 - n)) {
            System.out.println("Числа одинакого близки от 10.");
        } else if (abs(10 - m) > abs(10 - n)) {
            System.out.println("Число n ближе к 10 чем число m.");
        } else {
            System.out.println("Число m ближе к 10 чем число n.");
        }


    }
}
