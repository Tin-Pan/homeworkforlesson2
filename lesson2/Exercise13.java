public class Exercise13 {
    public static void main(String[] args) {
        //задание:"Выведите на экран первые 11 членов последовательности Фибоначчи.
        // Напоминаем, что первый и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих."

        System.out.println("Последовательность Фибоначчи:");
        int a1 = 1;
        int a2 = 1;
        System.out.print(a1 + " ");
        System.out.print(a2 + " ");
        for (int i = 0; i < 9; i++) {
            int a = a1 + a2;
            System.out.print(a + " ");
            a1 = a2;
            a2 = a;
        }
    }
}
