public class Exercise5 {
    public static void main(String[] args) {
        //задание:"В три переменные a, b и c явно записаны программистом три целых попарно неравных между собой числа.
        //Создать программу, которая переставит числа в переменных таким образом, чтобы при выводе на экран последовательность a, b и c оказалась строго возрастающей."

        int a = -7;
        int b = -65;
        int c = 8;

        int min;
        int average;
        int max;
        if (a < b && a < c) {
            min = a;
            if (b < c) {
                average = b;
                max = c;
            } else {
                average = c;
                max = b;
            }
        } else if (b < c) {
            min = b;
            if (a < c) {
                average = a;
                max = c;
            } else {
                average = c;
                max = a;
            }
        } else {
            min = c;
            if (a < b) {
                average = a;
                max = b;
            } else {
                average = b;
                max = a;
            }
        }

        System.out.println("Числа в переменных a, b и c: "+a+", "+b+", "+c);
        System.out.println("Возрастающая последовательность: "+min+", "+average+", "+max);

    }
}
